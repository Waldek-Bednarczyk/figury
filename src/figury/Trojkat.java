/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figury;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.Polygon;

/**
 *
 * @author Waldi
 */
public class Trojkat extends Figura {
    public Trojkat(Graphics2D buf, int del, int w, int h)
        {
            super(buf, del, w, h);
            shape = new Polygon(new int[]{-40,40,0}, new int[]{0,0,60},3);
            aft = new AffineTransform();                                  
            area = new Area(shape);
        }   
}
