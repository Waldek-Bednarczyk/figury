/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figury;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;


/**
 *
 * @author Waldi
 */
public class Kwadrat extends Figura {
    
    public Kwadrat(Graphics2D buf, int del, int w, int h)
    {
        super(buf, del, w, h);
        shape = new Rectangle2D.Float(0, 0, 80, 80);
	aft = new AffineTransform();                                  
	area = new Area(shape);
    }
}